# Installation

`pip install maas`

# Configuration

1. Run `python manage.py migrate`
2. Follow steps below for configuration settings for individual mail as a service providers

## General Settings

```
MAAS = {
  'DEFAULT_SENDER': 'default_sender@mailprovider.com',
  'DEFAULT_SENDER_TITLE': 'Default Sender'
}
```

## Mailgun API V3

In your `settings.py`, add the following:

```
MAAS = {
  'PROVIDER': 'MAILGUN_V3',
  'DOMAIN': 'your.configured.domain',
  'API_KEY': 'YOUR_API_KEY',
  'OPTIONS': {
    'context_processors': [
      'your_module.your_context_processing_function'
    ]
  }
}
```

# Usage

## Simple Creation

```
from maas.models import Mail

mail = Mail(
  sender='sender@domain.com', 
  sender_title='Sender From Domain.com',
  recipient='recipient@domain.com',
  subject='Test Subject',
  body='Test Body'
)
```

## Creation from Templates

```
from maas.models import Mail

mail = Mail(
  sender='sender@domain.com', 
  sender_title='Sender From Domain.com',
  recipient='recipient@domain.com',
  subject='Test Subject',
  template='template_to_load.html', # uses Django template loader
  context={                         # context is optional
    'context': 'passed to template'
  }
)
```

## Sending Mail

To send mail, run `mail.send()`.  Note that this will also trigger `mail.send()` once the message has been successfully sent.